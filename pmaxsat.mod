param s, integer, > 0;
param n, integer, > 0;

set S{1..s};

var x{1..n}, binary;

var y{1..s}, binary, >= 0;

s.t. c{i in 1..s}:
     sum{j in S[i]} (if j > 0 then x[j] else (1-x[-j])) >= y[i];


maximize soft: sum{i in 1..s} y[i];

solve;

printf '%s of %g clauses satisfied\n', sum{i in 1..s} y[i], s;

end;
