import sys
import os

if len(sys.argv) < 3:
  print("usage python parser.py <input_folder> <output_folder>")
  exit()


for filename in os.listdir(sys.argv[1]):
    soft = []
    n = 0
    file_in = open(sys.argv[1] + "/" + filename, "r")

    for line in file_in:
      if (line[0] == 'c'):
        continue
      if (line[0] == 'p'):
        n = line.split(' ')[2]
        continue
      if (line[0] == '%'):
        break

      clause = line.rpartition(' ')[0]
      soft.append(clause)

    filename_out = filename.replace(".cnf", ".dat")
    file_out = open(sys.argv[2] + "/" + filename_out, "w")
    file_out.write("data;\n")
    file_out.write("param s := {0};\n".format(len(soft)))
    file_out.write("param n := {0};\n".format(n))
    for i in range(0,len(soft)):
      file_out.write("set S[{0}] := {1};\n".format(i+1, soft[i]))

    file_out.write("end;")
    file_out.close()
