import sys
import os

timeout = 3600

if len(sys.argv) < 3:
  print("usage python parser.py <input_folder> <output_folder>")
  exit()


for filename in os.listdir(sys.argv[1]):
    if not os.path.exists(sys.argv[2]):
        os.makedirs(sys.argv[2])
    filename_out = filename.replace(".cnf", ".out")
    sys_call = "glpsol -m pmaxsat.mod -d {0} --tmlim {1} > {2}".format(sys.argv[1] + "/"+ filename, timeout, sys.argv[2] + "/"+ filename_out)
    os.system(sys_call)
