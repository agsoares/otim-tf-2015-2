#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <iostream>
#include <sstream>
#include <fstream>
#include <ctime>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef vector<int> vi;
typedef vector<bool> vb;

int var_n;
int clause_n;

vb vars;
vector<vi> clauses;

int best;
double best_time;
int actual_best;

int verify_clause(vi clause, int pos, bool val) {
    for (int i = 0; i < clause.size(); i++) {
        if (!val && clause[i] == -pos) return 1;
        if (val  && clause[i] ==  pos) return 1;
    }
    return 0;
}

int verify_clause(vi clause) {
    for (int i = 0; i < clause.size(); i++) {
        if ( vars[abs(clause[i])] && clause[i] > 0) return 1;
        if (!vars[abs(clause[i])] && clause[i] < 0) return 1;
    }
    return 0;
}

void generate_solution() {
    for (int i = 1; i < vars.size(); i++) {
        int true_count = 0, false_count = 0;
        for (int j = 0; j < clauses.size(); j++) {
            true_count  += verify_clause(clauses[j], i, true);
            false_count += verify_clause(clauses[j], i, false);
        }
        vars[i] = true_count >= false_count ? true : false;
    }
}

int verify_solution(vb vars) {
    int count = 0;
    for (int i = 0; i < clauses.size(); i++) {
        count += verify_clause(clauses[i]);
    }
    return count;
}

int main(int argc, char** argv) {
    srand(time(NULL));
    double start_time = time(NULL);
    ifstream file;

    double initial_temp = 120.0;
    double stop_temp = 0.00008;
    double cooling = 0.9;
    const double e = 2.718281828;

    int max_iterations = 200;

    //ARGS PARSING
    for (int a = 1; a < argc; a++) {
        if (strcmp(argv[a], "-f") == 0) {
            file.open(argv[++a]);
            if (!file.is_open()) {
                cout << "error: file not found" << endl;
                return -1;
            }
        }
        if (strcmp(argv[a], "-i") == 0) {
            initial_temp = atof(argv[++a]);
        }
        if (strcmp(argv[a], "-e") == 0) {
            stop_temp = atof(argv[++a]);
        }
        if (strcmp(argv[a], "-a") == 0) {
            cooling = atof(argv[++a]);
        }
    }

    if (!file.is_open()) {
        cout << "error: missing parameter -f <file>" << endl;
        return -1;
    }
    //FILE READING
    int i = 0;
    for (string line; !getline(file, line).eof(); ) {
        if (line[0] == '%' || line[0] == '0') break;
        if (line[0] == 'c') continue;
        if (line[0] == 'p') {
            istringstream is(line);
            is.ignore(256, ' ');
            is.ignore(256, ' ');
            is >> var_n >> clause_n;
            vars.assign(var_n+1, true);
            clauses.assign(clause_n, vi());
            continue;
        }
        istringstream is(line);
        for (int n; is >> n && n != 0; ) {
            clauses[i].push_back(n);
        }
        i++;
    }
    file.close();
    generate_solution();

    actual_best = verify_solution(vars);
    best = actual_best;
    cout  << "Initial Solution: " << best << endl;
    for (double T = initial_temp; T > stop_temp; T *= cooling) {
        for (int i = 0; i < max_iterations; i++) {
            int flip_pos = (rand()%var_n)+1;
            vars[flip_pos] = !vars[flip_pos];
            int fitness  = verify_solution(vars);
            double prob  = rand()/(double)RAND_MAX;
            double delta = pow(e, (double)(fitness-best)/T);
            if (fitness >= actual_best || prob < delta) {
                actual_best = fitness;
                //cout << actual_best << " " << best << " " << prob << " " << delta << " " << T << endl;
                if (actual_best > best)  {
                    best = actual_best;
                    best_time = (time(NULL) - start_time);
                }

            } else {
                vars[flip_pos] = !vars[flip_pos];
                //cout << "NAY" << endl;
            }
            if (clauses.size() == best) break;
        }
        if (clauses.size() == best) break;
    }
    cout  << "Best Solution: " << best;
    cout  << " of " << clause_n << endl;


    return 0;
}
