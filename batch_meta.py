import sys
import os

times_to_run = 5

param = "-i 120.0 -e 0.00008 -a 0.9"

for filename in os.listdir(sys.argv[1]):
    if not os.path.exists(sys.argv[2]):
        os.makedirs(sys.argv[2])
    for i in range(0, times_to_run):
        filename_out = filename.replace(".cnf", "-" + str(i) + ".out")
        sys_call = "(time ./bin/main -f {0} {1}) 1> {2} 2>> {2}".format(sys.argv[1]+"/"+filename, param,sys.argv[2]+"/"+filename_out)
        os.system(sys_call)
