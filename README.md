# README #

* This is an academic implementation of the Simmulated Annealing Metaheuristics Algorithm for the discipline of Combinatorial Optimization registered under the code INF05010 in the Federal University of Rio Grande do Sul. 
* Version 1.0
* Developed by Adriano Guimarães Soares, Rafael Franciosi Petersen